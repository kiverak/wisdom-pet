package com.gitlab.kiverak.wisdompet.web.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer {

    private Long id;
    private String firstName;
    private String lastName;
    private String emailAdress;
    private String phoneNumber;
    private String address;
}
