package com.gitlab.kiverak.wisdompet.data.repositories;

import com.gitlab.kiverak.wisdompet.data.entities.CustomerEntity;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<CustomerEntity, Long> {

    CustomerEntity findByEmail(String email);

}
