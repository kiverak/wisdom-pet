package com.gitlab.kiverak.wisdompet.services;

import com.gitlab.kiverak.wisdompet.data.entities.CustomerEntity;
import com.gitlab.kiverak.wisdompet.data.repositories.CustomerRepository;
import com.gitlab.kiverak.wisdompet.web.errors.NotFoundException;
import com.gitlab.kiverak.wisdompet.web.models.Customer;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getAllCustomers(String filterEmail) {
        List<Customer> customers = new ArrayList<>();
        if (StringUtils.hasLength(filterEmail)) {
            CustomerEntity entity = customerRepository.findByEmail(filterEmail);
            customers.add(translateDbToWeb(entity));
        } else {
            Iterable<CustomerEntity> entities = customerRepository.findAll();
            entities.forEach(entity -> {
                customers.add(translateDbToWeb(entity));
            });
        }

        return customers;
    }

    public Customer getCustomer(long id) {
        Optional<CustomerEntity> optional = customerRepository.findById(id);
        if (optional.isEmpty()) {
            throw new NotFoundException("customer fot found with id");
        }
        return translateDbToWeb(optional.get());
    }

    public Customer createOrUpdate(Customer customer) {
        CustomerEntity entity = translateWebToDb(customer);
        entity = customerRepository.save(entity);
        return translateDbToWeb(entity);
    }

    public void deleteCustomer(long id) {
        customerRepository.deleteById(id);
    }

    private CustomerEntity translateWebToDb(Customer customer) {
        CustomerEntity entity = new CustomerEntity();
        entity.setId(customer.getId());
        entity.setFirstName(customer.getFirstName());
        entity.setLastName(customer.getLastName());
        entity.setEmail(customer.getEmailAdress());
        entity.setPhone(customer.getPhoneNumber());
        entity.setAddress(customer.getAddress());
        return entity;
    }

    private Customer translateDbToWeb(CustomerEntity entity) {
        return new Customer(entity.getId(), entity.getFirstName(), entity.getLastName(), entity.getEmail(),
                entity.getPhone(), entity.getAddress());
    }
}
